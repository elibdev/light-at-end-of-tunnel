(defproject quil-workflow "1.0.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [quil "2.2.6"]]
  :injections [(require 
                 '(clojure.tools.namespace
                   [repl :as repl :refer [refresh]] 
                   [find :as find]))])
