(ns sketch.dynamic
  (:require [quil.core :as q]))

(def title "End of the Tunnel")

(def size [800 600])

(defn setup []
  ;; set shared global state
  (q/smooth)
  (q/frame-rate 20)
  (q/background 0)
  (q/no-stroke)
  ;; return initial state
  {:radius 0
   :x 0
   :y 0})

(defn evolve [state]
  ;; update state to use new random values
  (assoc state :radius (rand 2)
               :x (rand-int (q/width))
               :y (rand-int (q/height))))

(defn draw [state]
  (q/fill 255 255 255)
  (apply q/ellipse (map state [:x :y :radius :radius])))
